

let assets = [
	{
	id: "1",
	name: "Maserati",
	description: "Supercool car.",
	stock: 8,
	isAvailable: true,
	dateAdded: "January 3, 2015"
	},
	{
	id: "2",
	name: "Lamborghini",
	description: "Super-supercool car.",
	stock: 2,
	isAvailable: true,
	dateAdded: "August 27, 2020"
	}
];

console.log(assets);


//JSON - Javascript Object Notation
/* Strings are easier to transfer from one application to another

JSON is a string.
JS Object is a object
JSON Keys are surrounded by double quotes.
JSON extra comma at the end will become an error
*/

//JSON sample
let jsonSample = `{
	"sampleKey1": "ValueA",
	"sampleKey2": "ValueB",
	"sampleKey3": 1,
	"sampleKey4": true
}`


console.log(typeof jsonSample);
let jsonConvert = JSON.parse(jsonSample);
console.log(typeof jsonConvert);
console.log(jsonConvert);

/* JSON is not only used in JS but also in other programming languages.
This is why iti s specified as Javascript Object Notation.
There are also JSON that are saved in a file with extension called .json

There is also a way to turn JSON into JS Objects and turn JS Objects into JSON */

/*Converting JS Objects into JSON

	This will allow us to convert/turn JS OBjects into JSON
	This is how JSON is manipulated even when received by other applications*/

	let batches = [
	{
		batch: "Batch 152"
	},
	{
		batch: "Batch 156"
	}
	];

	//To turn JS Objects into JSON we use the method JSON.stringify()
	//This method will return JSON format out of the object that we pass as an argument

	let batchesJSON = JSON.stringify(batches);
	console.log(batchesJSON);

let data = {
	name: "Katniss",
	age: 20,
	address: {
		city: "Kansas City",
		state: "Kansas"
	}
}
let dataJSON = JSON.stringify(data);
console.log(dataJSON);

/*JSON.stringify is commonly used when trying to pass data from one application to another via HTTP 
requests are a request for data between the client(browser/page/app) and a server.*/

let data2 = {
	username: "saitamaOPM",
	password: "onepuuuuuunch",
	isAdmin: true
}

let data3 = {
	username: "lightYagami",
	password: "notKiraDefinitely",
	isAdmin: false
}

let data4 = {
	username: "Llawliett",
	password: "yagamiiskira07",
	isAdmin: false
}


let data2JSON = JSON.stringify(data2);
let data3JSON = JSON.stringify(data3);
let data4JSON = JSON.stringify(data4);
let assetsarrayJSON = JSON.stringify(assets);


console.log(data2JSON);
console.log(data3JSON);
console.log(data4JSON);
console.log(assetsarrayJSON);


let items = `[
{
	"id": "show-1",
	"name": "Doritos",
	"stock": 10,
	"price": 150
},
{
	"id": "show-2",
	"name": "Piattos",
	"stock": 16,
	"price": 12
}
]`


let itemsJSArr = JSON.parse(items);
console.log(itemsJSArr);
itemsJSArr.pop();
console.log(itemsJSArr);

items = JSON.stringify(itemsJSArr);
console.log(items);


/* Server - page/browser (Client)

-Client sends data in JSON format
-Server receives data in JSON format
	- To be able to manipulate/process the data sent
		-parse the JSON back into JS object
	- stringify the data back into JSON
	- send it back to the Client (page/browser)
-Client receives data in JSON format */

/* ----
	end user inputs - JSON format to the server
	server - receives input in JSON format
		   - converts the user input into objects using stringify() function

*/


let courses = `[
{
	"name": "Math 101",
	"description": "Learn the basics of Math",
	"price": 2500
},
{
	"name": "Science 101",
	"description": "Learn the basics of Science",
	"price": 2700
}
]`


console.log(courses);
let coursesJSArr = JSON.parse(courses);
coursesJSArr.pop();
courses = JSON.stringify(coursesJSArr);
console.log(courses);


let coursesJSArr2 = JSON.parse(courses);
coursesJSArr2.push({
	name: "Filipino 101",
	description: "Learn the basics of Filipino",
	price: 1900
}
);
courses = JSON.stringify(coursesJSArr2);
console.log(courses);
