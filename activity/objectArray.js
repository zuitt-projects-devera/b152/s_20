//courses array of objects
let courses = [];

//class constructor for courses array of objects
class Course {
		constructor(id, name, description, price, isActive) {
			this.id = id;
			this.name = name;
			this.description = description;
			this.price = price;
			this.price = false;
		}
	}


/* postCourse Function - adds object to the courses array of objects*/
	const postCourse = (id, name, description, price, isActive) => {
		courses.push({
			id: "course-" + id, 
			name: name, 
			description: description, 
			price: price,
			isActive: isActive
		});
		alert(`You have created ${name}. The Price is ${price}`)
	}
	postCourse(1, "Javascript For Beginners", "Basic Javascript Programming", 300, true);
	postCourse(2, "PHP For Beginners", "Basic PHP Programming", 250, true);
	postCourse(3, "Java For Beginners", "Basic Java Programming", 500, true);
	console.log(courses);


/* getSingleCourse Function - fetches a single object using id from 
courses array of object and displays its values*/
	const getSingleCourse = (id) => {
		let singleData = courses.find((_courses) => {
			//course or the parameter in the anonymous function is the current item being looped 
			//by the find() method

			/*if the anonymous function in find(), returns true, then the find() method will stop and 
			return the current item being looped. */
			return _courses.id === "course-" + id;
		})
		if (singleData !== undefined) {
			console.log(singleData);
		}
		else {
			console.log(`No course yet that contains the id: course-${id}`);
		}

	}
	
getSingleCourse(5);

/*deleteCourse function - deletes the last object from the courses array of objects*/
	const deleteCourse = () => {
		courses.pop();
		console.log(courses);
	};
deleteCourse();
